﻿namespace Models.FixedFee
{
    public class FixedFeeVM
    {
        public string FeeName { get; set; }
        public float FeeAmount { get; set; }
    }
}
