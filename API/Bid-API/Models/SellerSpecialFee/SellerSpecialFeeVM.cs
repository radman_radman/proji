﻿namespace Models.SellerSpecialFee
{
    public class SellerSpecialFeeVM
    {
        public int VehicleTypeId { get; set; }
        public float FeePercentage { get; set; }
    }
}
