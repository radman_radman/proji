﻿namespace Models.AssociationCost
{
    public class AssociationCostVM
    {
        public float MinPrice { get; set; }
        public float? MaxPrice { get; set; }
        public float Cost { get; set; }
    }
}
