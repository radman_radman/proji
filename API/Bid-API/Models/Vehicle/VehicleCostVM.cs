﻿namespace Models.Vehicle
{
    public class VehicleCostVM
    {
        public float VehiclePrice { get; set; }
        public string VehicleTypeName { get; set; }
        public float BasicUserFees { get; set; }
        public float SellerSpecialFees { get; set; }
        public float AssociationFees { get; set; }
        public float StorageFees { get; set; }

        public float Total => VehiclePrice + BasicUserFees + SellerSpecialFees + AssociationFees + StorageFees;
    }
}
