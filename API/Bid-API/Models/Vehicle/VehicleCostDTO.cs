﻿namespace Models.Vehicle
{
    /// <summary>
    /// For data that we are getting as an input
    /// we create a model that its name is ending with DTO (Data Transfer Option)
    /// </summary>
    public class VehicleCostDTO
    {
        public int VehicleTypeId { get; set; }
        public float VehiclePrice { get; set; }
    }
}
