﻿namespace Models.BasicUserFee
{
    public class BasicUserFeeVM
    {
        public int VehicleTypeId { get; set; }
        public float FeePercentage { get; set; }
        public float MinFee { get; set; }
        public float MaxFee { get; set; }
    }
}
