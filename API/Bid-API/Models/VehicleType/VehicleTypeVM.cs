﻿namespace Models.VehicleType
{
    public class VehicleTypeVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
