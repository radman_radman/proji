﻿using Microsoft.AspNetCore.Mvc;
using Services.VehicleType;

namespace Bid_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleTypeController : ControllerBase
    {
        private readonly IVehicleTypeService _vehicleTypeService;

        public VehicleTypeController(IVehicleTypeService vehicleTypeService)
        {
            _vehicleTypeService = vehicleTypeService;
        }


        [HttpGet]
        public async Task<IActionResult> GetAll(CancellationToken cancellationToken = default)
        {
            var result = await _vehicleTypeService.GetVehicleTypesAsync(cancellationToken);

            if (result.Success)
                return Ok(result);

            return BadRequest(result);
        }


        //
        // Other actions for other actions
        // Put/Patch for update
        // Post for creating
        // Delete for deleting data
        // and ...
        //

    }
}
