﻿using Microsoft.AspNetCore.Mvc;
using Models.Vehicle;
using Services.Vehicle;

namespace Bid_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleService _vehicleService;

        public VehicleController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }

        [HttpGet("Costs")]
        public async Task<IActionResult> CalculateCostsAsync([FromQuery] VehicleCostDTO vehicleCostDTO, CancellationToken cancellationToken = default)
        {
            var result = await _vehicleService.CalculateCostsAsync(vehicleCostDTO, cancellationToken);

            if (result.Success)
                return Ok(result);

            return BadRequest(result);
        }
    }
}
