
using Helpers.RegisterExtensions;
using Services.Common;

namespace Bid_API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();


            #region Regestering Services
            // Instead of adding services one by one and write several lines of code only for registering them,
            // we add them by this method all at once
            builder.Services.RegisterScoped<IScopedService>(typeof(IScopedService).Assembly);
            #endregion

            #region CORS
            builder.Services.AddCors(z =>
            {
                z.AddPolicy("Policy", z =>
                {
                    z.AllowAnyOrigin();
                    z.AllowAnyHeader();
                    z.AllowAnyMethod();
                });
            });
            #endregion

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.UseCors("Policy");

            app.MapControllers();

            app.Run();
        }
    }
}
