﻿using Models.BasicUserFee;
using Services.Common;

namespace Services.BasicUserFee
{
    public interface IBasicUserFeeService
    {
        Task<ServiceResult<List<BasicUserFeeVM>>> GetBasicUserFeesAsync(CancellationToken cancellationToken = default);
    }
}