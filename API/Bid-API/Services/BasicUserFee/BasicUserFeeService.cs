﻿using Models.BasicUserFee;
using Services.Common;

namespace Services.BasicUserFee
{
    /// <summary>
    /// Service for working with data related to BasicUserFee Entity
    /// </summary>
    public class BasicUserFeeService : IBasicUserFeeService, IScopedService
    {
        //
        // In real world we do connect to a database and fetch data from it 
        // for simplicity, here I use some hard coded data as the data fetched from the database
        //

        private readonly List<(int vehicleTypeId, float feePercentage, float minFee, float maxFee)> basicUserFees;

        public BasicUserFeeService()
        {
            basicUserFees = new List<(int, float, float, float)>
            {
                (1, 10, 10, 50),
                (2, 10 , 10, 200)
            };
        }

        public async Task<ServiceResult<List<BasicUserFeeVM>>> GetBasicUserFeesAsync(CancellationToken cancellationToken = default)
        {
            var result = basicUserFees.Select(z => new BasicUserFeeVM
            {
                VehicleTypeId = z.vehicleTypeId,
                FeePercentage = z.feePercentage,
                MinFee = z.minFee,
                MaxFee = z.maxFee
            }).ToList();

            return ServiceResult<List<BasicUserFeeVM>>.SuccessResult(result);
        }
    }
}