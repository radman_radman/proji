﻿using Models.SellerSpecialFee;
using Services.Common;

namespace Services.SellerSpecialFee
{
    /// <summary>
    /// Service for working with data related to SellerSpecialFee Entity
    /// </summary>
    public class SellerSpecialFeeService : ISellerSpecialFeeService, IScopedService
    {

        //
        // In real world we do connect to a database and fetch data from it 
        // for simplicity, here I use some hard coded data as the data fetched from the database
        //
        private readonly List<(int vehicleTypeId, float feePercentage)> sellerSpecialFees;

        public SellerSpecialFeeService()
        {
            sellerSpecialFees = new List<(int, float)>
            {
                (1, 2),
                (2, 4)
            };
        }

        public async Task<ServiceResult<List<SellerSpecialFeeVM>>> GetSellerSpecialFeesAsync(CancellationToken cancellationToken = default)
        {
            var result = sellerSpecialFees.Select(z => new SellerSpecialFeeVM
            {
                VehicleTypeId = z.vehicleTypeId,
                FeePercentage = z.feePercentage
            }).ToList();

            return ServiceResult<List<SellerSpecialFeeVM>>.SuccessResult(result);
        }
    }
}