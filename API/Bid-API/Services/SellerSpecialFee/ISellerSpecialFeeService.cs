﻿using Models.SellerSpecialFee;
using Services.Common;

namespace Services.SellerSpecialFee
{
    public interface ISellerSpecialFeeService
    {
        Task<ServiceResult<List<SellerSpecialFeeVM>>> GetSellerSpecialFeesAsync(CancellationToken cancellationToken = default);
    }
}