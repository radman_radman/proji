﻿using Models.AssociationCost;
using Services.Common;

namespace Services.AssociationCost
{
    public interface IAssociationCostService
    {
        Task<ServiceResult<List<AssociationCostVM>>> GetAssociationCostsAsync(CancellationToken cancellationToken = default);
    }
}