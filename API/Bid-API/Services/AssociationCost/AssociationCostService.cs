﻿using Models.AssociationCost;
using Services.Common;

namespace Services.AssociationCost
{
    /// <summary>
    /// Service for working with data related to AssociationCost Entity
    /// </summary>
    public class AssociationCostService : IAssociationCostService, IScopedService
    {

        //
        // In real world we do connect to a database and fetch data from it 
        // for simplicity, here I use some hard coded data as the data fetched from the database
        //
        private readonly List<(float minPrice, float? maxPrice, float cost)> associationCosts;

        public AssociationCostService()
        {
            associationCosts = new List<(float, float?, float)>
            {
                (1, 500, 5),
                (501, 1000, 10),
                (1001, 3000, 15),
                (3001, null, 20)
            };
        }

        public async Task<ServiceResult<List<AssociationCostVM>>> GetAssociationCostsAsync(CancellationToken cancellationToken = default)
        {
            var result = associationCosts.Select(z => new AssociationCostVM
            {
                MinPrice = z.minPrice,
                MaxPrice = z.maxPrice,
                Cost = z.cost
            }).ToList();

            return ServiceResult<List<AssociationCostVM>>.SuccessResult(result);
        }
    }
}