﻿namespace Services.Common
{
    /// <summary>
    /// Just an interface that says the inherited service will be injected per scope
    /// </summary>
    public interface IScopedService
    {
    }
}
