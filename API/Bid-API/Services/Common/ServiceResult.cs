﻿namespace Services.Common
{
    public class ServiceResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }

        // Factory method for a successful result
        public static ServiceResult SuccessResult(string message = null)
        {
            return new ServiceResult
            {
                Success = true,
                Message = message,
            };
        }

        // Factory method for a failure result
        public static ServiceResult FailureResult(string message)
        {
            return new ServiceResult
            {
                Success = false,
                Message = message
            };
        }
    }

    public class ServiceResult<T> : ServiceResult where T : class
    {

        public T Data { get; set; }

        // Factory method for a successful result
        public static ServiceResult<T> SuccessResult(T data, string message = null)
        {
            return new ServiceResult<T>
            {
                Success = true,
                Message = message,
                Data = data
            };
        }

        // Factory method for a failure result
        public static ServiceResult<T> FailureResult(string message)
        {
            return new ServiceResult<T>
            {
                Success = false,
                Message = message
            };
        }
    }
}
