﻿using Models.FixedFee;
using Services.Common;

namespace Services.FixedFee
{
    public interface IFixedFeeService
    {
        Task<ServiceResult<FixedFeeVM>> GetFixedFeesAsync(CancellationToken cancellationToken = default);
    }
}