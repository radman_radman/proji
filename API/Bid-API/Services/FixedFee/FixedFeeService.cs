﻿using Models.FixedFee;
using Services.Common;

namespace Services.FixedFee
{
    /// <summary>
    /// Service for working with data related to FixedFee Entity
    /// </summary>
    public class FixedFeeService : IFixedFeeService, IScopedService
    {
        //
        // In real world we do connect to a database and fetch data from it 
        // for simplicity, here I use some hard coded data as the data fetched from the database
        //

        private readonly List<(string feeName, float feeAmount)> fixedFees;

        public FixedFeeService()
        {
            fixedFees = new List<(string, float)>
            {
                ("Storage", 100f)
            };
        }

        public async Task<ServiceResult<FixedFeeVM>> GetFixedFeesAsync(CancellationToken cancellationToken = default)
        {
            var result = fixedFees.Select(z => new FixedFeeVM
            {
                FeeName = z.feeName,
                FeeAmount = z.feeAmount
            }).FirstOrDefault();

            return ServiceResult<FixedFeeVM>.SuccessResult(result);
        }
    }
}