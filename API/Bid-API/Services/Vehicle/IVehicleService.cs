﻿using Models.Vehicle;
using Services.Common;

namespace Services.Vehicle
{
    public interface IVehicleService
    {
        Task<ServiceResult<VehicleCostVM>> CalculateCostsAsync(VehicleCostDTO vehicleCostDTO, CancellationToken cancellationToken);
    }
}