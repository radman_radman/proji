﻿using Models.AssociationCost;
using Models.Vehicle;
using Services.AssociationCost;
using Services.BasicUserFee;
using Services.Common;
using Services.FixedFee;
using Services.SellerSpecialFee;
using Services.VehicleType;

namespace Services.Vehicle
{
    public class VehicleService : IVehicleService, IScopedService
    {
        private readonly IBasicUserFeeService _basicUserFeeService;
        private readonly ISellerSpecialFeeService _sellerSpecialFeeService;
        private readonly IAssociationCostService _associationCostService;
        private readonly IFixedFeeService _fixedFeeService;
        private readonly IVehicleTypeService _vehicleTypeService;

        public VehicleService(
            IBasicUserFeeService basicUserFeeService,
            ISellerSpecialFeeService sellerSpecialFeeService,
            IAssociationCostService associationCostService,
            IFixedFeeService fixedFeeService,
            IVehicleTypeService vehicleTypeService)
        {
            _basicUserFeeService = basicUserFeeService;
            _sellerSpecialFeeService = sellerSpecialFeeService;
            _associationCostService = associationCostService;
            _fixedFeeService = fixedFeeService;
            _vehicleTypeService = vehicleTypeService;
        }
        public async Task<ServiceResult<VehicleCostVM>> CalculateCostsAsync(VehicleCostDTO vehicleCostDTO, CancellationToken cancellationToken = default)
        {
            if (vehicleCostDTO.VehiclePrice <= 0)
                return ServiceResult<VehicleCostVM>.FailureResult("Vehicle price cannot be zero or negative");

            var basicUserFeeResult = await _basicUserFeeService.GetBasicUserFeesAsync(cancellationToken);
            var sellerSpecialFeeResult = await _sellerSpecialFeeService.GetSellerSpecialFeesAsync(cancellationToken);
            var associationCostResult = await _associationCostService.GetAssociationCostsAsync(cancellationToken);
            var fixedFeeResult = await _fixedFeeService.GetFixedFeesAsync(cancellationToken);
            var vehicleTypeResult = await _vehicleTypeService.GetVehicleTypeAsync(vehicleCostDTO.VehicleTypeId, cancellationToken);

            if (!basicUserFeeResult.Success || !sellerSpecialFeeResult.Success || !associationCostResult.Success || !fixedFeeResult.Success || !vehicleTypeResult.Success)
                return ServiceResult<VehicleCostVM>.FailureResult("Failed to retrieve data from services.");


            //
            // In real life this is NOT efficient and we will write this query in our database such as SQL 
            // for simplicity and not having a database and entities I am writing this in this way
            //
            // In production I will never load all tables into RAM and then join them and select from them
            // All should be done in database and only the final result should be fetched into RAM
            // A StoredProcedure can do this or this can be done with Dapper as well
            //
            var costs = (from bus in basicUserFeeResult.Data
                         join spf in sellerSpecialFeeResult.Data
                         on bus.VehicleTypeId equals spf.VehicleTypeId
                         where bus.VehicleTypeId == vehicleCostDTO.VehicleTypeId
                         select new
                         {
                             basicFeePecentage = bus.FeePercentage,
                             basicMinFee = bus.MinFee,
                             basicMaxFee = bus.MaxFee,
                             specialFeesPercentage = spf.FeePercentage
                         }).FirstOrDefault();



            var result = new VehicleCostVM
            {
                VehicleTypeName = vehicleTypeResult.Data.Name,
                StorageFees = fixedFeeResult.Data.FeeAmount,
                SellerSpecialFees = (float)Math.Round(vehicleCostDTO.VehiclePrice * (costs.specialFeesPercentage / 100), 2),
                VehiclePrice = (float)Math.Round(vehicleCostDTO.VehiclePrice, 2),
                AssociationFees = (float)Math.Round(CalculateAssociationFees(associationCostResult.Data, vehicleCostDTO.VehiclePrice), 2),
                BasicUserFees = (float)Math.Round(CalculateBasicPrice(costs.basicFeePecentage, costs.basicMinFee, costs.basicMaxFee, vehicleCostDTO.VehiclePrice), 2)
            };

            return ServiceResult<VehicleCostVM>.SuccessResult(result);
        }


        #region Helpers
        private float CalculateBasicPrice(float percentage, float min, float max, float vehiclePrice)
        {
            var basicFee = vehiclePrice * (percentage / 100);

            if (basicFee < min)
                return min;

            if (basicFee > max)
                return max;

            return basicFee;
        }


        private float CalculateAssociationFees(List<AssociationCostVM> associationCosts, float vehiclePrice)
        {
            return associationCosts.First(z => vehiclePrice >= z.MinPrice && vehiclePrice < (z.MaxPrice ?? float.MaxValue)).Cost;
        }
        #endregion
    }
}