﻿using Models.VehicleType;
using Services.Common;

namespace Services.VehicleType
{
    public interface IVehicleTypeService
    {
        Task<ServiceResult<VehicleTypeVM>> GetVehicleTypeAsync(int id, CancellationToken cancellationToken);
        Task<ServiceResult<List<VehicleTypeVM>>> GetVehicleTypesAsync(CancellationToken cancellationToken = default);
    }
}