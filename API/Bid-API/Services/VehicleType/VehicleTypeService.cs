﻿using Models.VehicleType;
using Services.Common;

namespace Services.VehicleType
{
    /// <summary>
    /// Service for working with data related to VehicleType Entity
    /// </summary>
    public class VehicleTypeService : IVehicleTypeService, IScopedService
    {
        //
        // In real world we do connect to a database and fetch data from it 
        // for simplicity, here I use some hard coded data as the data fetched from the database
        //


        private readonly List<(int id, string name)> vehicleTypes;


        // inject dependecies
        // since we do not have any dependency I just initialise the List for our data 
        public VehicleTypeService()
        {
            vehicleTypes = new List<(int id, string name)>
            {
                new (1,"Common"),
                new (2,"Luxury" )
            };
        }

        public async Task<ServiceResult<List<VehicleTypeVM>>> GetVehicleTypesAsync(CancellationToken cancellationToken = default)
        {
            // in this step we can use mappers such AutoMapper or our custom mapper to map the data
            // fetched from database into our view model
            // for simplicity I use LINQ select for the mapping process

            var result = vehicleTypes.Select(z => new VehicleTypeVM
            {
                Id = z.id,
                Name = z.name
            }).ToList();


            return ServiceResult<List<VehicleTypeVM>>.SuccessResult(result);
        }


        public async Task<ServiceResult<VehicleTypeVM>> GetVehicleTypeAsync(int id, CancellationToken cancellationToken)
        {
            var result = vehicleTypes.Select(z => new VehicleTypeVM
            {
                Id = z.id,
                Name = z.name
            }).FirstOrDefault(z => z.Id == id);

            if (result == null)
                return ServiceResult<VehicleTypeVM>.FailureResult("data with given Id was not found");


            return ServiceResult<VehicleTypeVM>.SuccessResult(result);
        }

    }
}


