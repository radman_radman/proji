﻿using Models.AssociationCost;
using Models.BasicUserFee;
using Models.FixedFee;
using Models.SellerSpecialFee;
using Models.Vehicle;
using Models.VehicleType;
using Moq;
using NUnit.Framework;
using Services.AssociationCost;
using Services.BasicUserFee;
using Services.Common;
using Services.FixedFee;
using Services.SellerSpecialFee;
using Services.Vehicle;
using Services.VehicleType;

namespace Tests
{
    [TestFixture]
    public class VehicleServiceTests
    {
        private Mock<IBasicUserFeeService> _basicUserFeeServiceMock;
        private Mock<ISellerSpecialFeeService> _sellerSpecialFeeServiceMock;
        private Mock<IAssociationCostService> _associationCostServiceMock;
        private Mock<IFixedFeeService> _fixedFeeServiceMock;
        private Mock<IVehicleTypeService> _vehicleTypeServiceMock;

        private VehicleService _vehicleService;

        [SetUp]
        public void Setup()
        {
            _basicUserFeeServiceMock = new Mock<IBasicUserFeeService>();
            _sellerSpecialFeeServiceMock = new Mock<ISellerSpecialFeeService>();
            _associationCostServiceMock = new Mock<IAssociationCostService>();
            _fixedFeeServiceMock = new Mock<IFixedFeeService>();
            _vehicleTypeServiceMock = new Mock<IVehicleTypeService>();

            _vehicleService = new VehicleService(
                _basicUserFeeServiceMock.Object,
                _sellerSpecialFeeServiceMock.Object,
                _associationCostServiceMock.Object,
                _fixedFeeServiceMock.Object,
                _vehicleTypeServiceMock.Object);
        }

        [Test]
        public async Task CalculateCostsAsync_ValidVehicleCostDTO_ReturnsCorrectVehicleCostVM()
        {
            // Arrange
            var vehicleCostDTO = new VehicleCostDTO
            {
                VehiclePrice = 398,
                VehicleTypeId = 1
            };

            var basicUserFeeData = new List<BasicUserFeeVM>
            {
                new BasicUserFeeVM { VehicleTypeId = 1, FeePercentage = 10, MinFee = 10, MaxFee = 50 }
            };
            var sellerSpecialFeeData = new List<SellerSpecialFeeVM>
            {
                new SellerSpecialFeeVM { VehicleTypeId = 1, FeePercentage = 2 }
            };
            var associationCostData = new List<AssociationCostVM>
            {
                new AssociationCostVM { MinPrice = 1, MaxPrice = 500, Cost = 5 }
            };
            var fixedFeeData = new FixedFeeVM { FeeName = "Storage", FeeAmount = 100 };

            var vehicleTypeData = new VehicleTypeVM { Id = 1, Name = "Common" };

            _basicUserFeeServiceMock.Setup(x => x.GetBasicUserFeesAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResult<List<BasicUserFeeVM>>.SuccessResult(basicUserFeeData));

            _sellerSpecialFeeServiceMock.Setup(x => x.GetSellerSpecialFeesAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResult<List<SellerSpecialFeeVM>>.SuccessResult(sellerSpecialFeeData));

            _associationCostServiceMock.Setup(x => x.GetAssociationCostsAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResult<List<AssociationCostVM>>.SuccessResult(associationCostData));

            _fixedFeeServiceMock.Setup(x => x.GetFixedFeesAsync(It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResult<FixedFeeVM>.SuccessResult(fixedFeeData));

            _vehicleTypeServiceMock.Setup(x => x.GetVehicleTypeAsync(vehicleCostDTO.VehicleTypeId, It.IsAny<CancellationToken>()))
                .ReturnsAsync(ServiceResult<VehicleTypeVM>.SuccessResult(vehicleTypeData));

            // Act
            var result = await _vehicleService.CalculateCostsAsync(vehicleCostDTO);

            // Assert
            Assert.That(result.Success, Is.True);
            Assert.That("Common", Is.EqualTo(result.Data.VehicleTypeName));
            Assert.That(398, Is.EqualTo(result.Data.VehiclePrice));
            Assert.That(39.8f, Is.EqualTo(result.Data.BasicUserFees));
            Assert.That(7.96f, Is.EqualTo(result.Data.SellerSpecialFees));
            Assert.That(5, Is.EqualTo(result.Data.AssociationFees));
            Assert.That(100, Is.EqualTo(result.Data.StorageFees));
        }

        [Test]
        public async Task CalculateCostsAsync_ZeroVehiclePrice_ReturnsFailureResult()
        {
            // Arrange
            var vehicleCostDTO = new VehicleCostDTO
            {
                VehiclePrice = 0,
                VehicleTypeId = 1
            };

            // Act
            var result = await _vehicleService.CalculateCostsAsync(vehicleCostDTO);

            // Assert
            Assert.That(result.Success, Is.False);
            Assert.That("Vehicle price cannot be zero or negative", Is.EqualTo(result.Message));
        }

        // Add more tests for edge cases, error scenarios, etc.

    }
}
