import { createContext } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
const AppContext = createContext();

export const AppProvider = ({ children }) => {
  return (
    <div className="h-100">
      <AppContext.Provider value={null}>{children}</AppContext.Provider>
    </div>
  );
};

export default AppContext;
