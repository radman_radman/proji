import axios from "axios";
import Swal from "sweetalert2";

const bidApiInstance = axios.create({
  baseURL: process.env.REACT_APP_BID_API_BASE_URL,
  withCredentials: false,
});

bidApiInstance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

bidApiInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response) {
      if (error.response.status === 404) window.location.href = "/NotFound";
      if (error.response.status === 401 || error.response.status === 403)
        window.location.href = "/Login"; // Lets say we got a Login page too

      Swal.fire({
        text: error.response.data.message,
        icon: "error",
      });
    }
  }
);

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  bidApiInstance: bidApiInstance,
};
