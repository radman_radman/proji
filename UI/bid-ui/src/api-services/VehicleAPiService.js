import httpClient from "./http-client.js";

const baseUrl = "/Vehicle";

const VehicleAPiService = {
  calculateCosts: function (model) {
    return httpClient.bidApiInstance.get(baseUrl + "/Costs", {
      params: model,
    });
  },
};

export default VehicleAPiService;
