import httpClient from "./http-client.js";

const baseUrl = "/VehicleType";

const VehicleTypeAPIService = {
  getVehicleTypes: function () {
    return httpClient.bidApiInstance.get(baseUrl);
  },
};

export default VehicleTypeAPIService;
