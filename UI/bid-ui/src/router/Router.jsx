import { createBrowserRouter } from "react-router-dom";

import Home from "../components/Home/Home";
import NotFound from "../components/NotFound/NotFound";

const router = createBrowserRouter([
  {
    path: "*",
    element: <NotFound />,
  },
  {
    path: "/",
    element: <Home />,
  },
]);

export default router;
