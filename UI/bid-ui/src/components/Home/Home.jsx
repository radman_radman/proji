import { useEffect, useState } from "react";
import VehicleTypeAPIService from "../../api-services/VehicleTypeAPIService";
import VehicleAPiService from "../../api-services/VehicleAPiService";
import Swal from "sweetalert2";
import "../../styles/gradiant.css";

export default function Home() {
  const [vehicleTypes, setVehicleTypes] = useState([]);
  const [vehiclePrice, setVehiclePrice] = useState();
  const [vehicleTypeId, setVehicleTypeId] = useState();
  const [costs, setCosts] = useState(null);

  useEffect(() => {
    VehicleTypeAPIService.getVehicleTypes()
      .then(({ data }) => {
        setVehicleTypes(data.data);
      })
      .catch((err) => {
        Swal.fire("Error", err.data.message);
      });
  }, []);

  const vehiclePriceOnChange = (event) => {
    const value = event.target.value;

    // Allow only numeric input (and an empty string, as required)
    setVehiclePrice(value.replace(/[^0-9.]/g, "").replace(/(\..*)\./g, "$1"));
  };

  const calculateCosts = (event) => {
    event.preventDefault();

    if (!vehicleTypeId || vehicleTypeId == null) {
      Swal.fire("Error", "Please select your vehicle type");
      return;
    }

    if (!vehiclePrice || vehiclePrice == "") {
      Swal.fire("Error", "Please enter your vehicle price");
      return;
    }
    VehicleAPiService.calculateCosts({
      vehicleTypeId: vehicleTypeId,
      vehiclePrice: vehiclePrice,
    }).then(({ data }) => {
      setCosts(data.data);
    });
  };
  return (
    <div className="gradient">
      <div className="container">
        <br />
        <br />
        <div className="rounded bg-light p-5">
          <h2>Car cost calculator</h2>
          <hr />
          <br />
          <form onSubmit={calculateCosts}>
            <div className="d-flex flex-wrap justify-content-around align-items-center ">
              <div>
                <input
                  type="text"
                  className="form-control"
                  value={vehiclePrice}
                  placeholder="Vehicle price"
                  onChange={vehiclePriceOnChange}
                />
                <small>Only possitive values are accepted</small>
              </div>
              <div>
                <select
                  className="form-select"
                  onChange={(e) => {
                    setVehicleTypeId(e.target.value);
                  }}
                >
                  <option defaultValue>Please select vehicle type</option>
                  {vehicleTypes.map((z) => (
                    <option value={z.id}>{z.name}</option>
                  ))}
                </select>
              </div>
              <button type="submit" className="btn btn-success m-2">
                Calculate Costs
              </button>
            </div>
          </form>

          <br />
          <br />
          <br />
          <div>
            <small>Results will display here :</small>
            <hr className="my-0" />

            {costs && (
              <div>
                <small className="fst-italic">All number are rounded with 2 digits</small>
                <div className="table-responsive mt-1">
                  <table className="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th
                          rowSpan="2"
                          className="text-center align-content-center"
                        >
                          Vehicle Price ($)
                        </th>
                        <th
                          rowSpan="2"
                          className="text-center align-content-center"
                        >
                          Vehicle Type
                        </th>
                        <th
                          colSpan="4"
                          className="text-center align-content-center"
                        >
                          Fees ($)
                        </th>
                        <th
                          rowSpan="2"
                          className="text-center align-content-center"
                        >
                          Total ($)
                        </th>
                      </tr>
                      <tr>
                        <th>Basic</th>
                        <th>Special</th>
                        <th>Association</th>
                        <th>Storage</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{costs.vehiclePrice}</td>
                        <td>{costs.vehicleTypeName}</td>
                        <td>{costs.basicUserFees}</td>
                        <td>{costs.sellerSpecialFees}</td>
                        <td>{costs.associationFees}</td>
                        <td>{costs.storageFees}</td>
                        <td>{costs.total}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
