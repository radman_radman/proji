import { Link } from "react-router-dom";

export default function NotFound() {
  return (
    <div className="text-center">
      <img
        src={require("../../assets/images/NotFound.jpg")}
        alt=""
        className="img-fluid"
        style={{ width: "750px" }}
      />
      <div className="text-center">
        <Link to={"/"}>
          <button className="btn btn-light-olive">Home</button>
        </Link>
      </div>
    </div>
  );
}
